import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { Signo } from 'src/app/_model/signo';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignoService } from 'src/app/_service/signo.service';
import { PacienteDialogComponent } from '../../paciente/paciente-dialog/paciente-dialog.component';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;
  fechaSeleccionada: Date;
  maxFecha: Date = new Date();
  pacientes: Paciente[];
  pacienteSeleccionado: Paciente;

  myControlPaciente: FormControl = new FormControl();

  pacientesFiltrados$: Observable<Paciente[]>;

  pacienteSubject: Subject<Paciente>  = new Subject<Paciente>();;

  constructor(
    private route: ActivatedRoute,
    private router: Router, 
    private signoService: SignoService, 
    private pacienteService: PacienteService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      id: new FormControl(0),
      paciente: this.myControlPaciente,
      fecha: new FormControl(new Date()),
      temperatura: new FormControl(''),
      pulso: new FormControl(''),
      ritmoRespiratorio: new FormControl('')
    });
    
    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = this.id != null;
      this.initForm();
    });

    this.pacienteSubject.asObservable().subscribe(data => {
      this.myControlPaciente.setValue(data);
    });

    this.listarPacientes();
    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  initForm() {
    if (this.edicion) {
      this.signoService.listarPorId(this.id).subscribe(data => {
        this.myControlPaciente.setValue(data.paciente);
        this.form = new FormGroup({
          id: new FormControl(data.idSigno),
          paciente: this.myControlPaciente,
          fecha: new FormControl(data.fecha),
          temperatura: new FormControl(data.temperatura),
          pulso: new FormControl(data.pulso),
          ritmoRespiratorio: new FormControl(data.ritmoRespiratorio)
        });
      });
    }
  }

  operar(): void {
    let signo = new Signo();
    signo.idSigno = this.form.value['id'];
    signo.paciente = this.form.value['paciente'];
    signo.fecha = this.form.value['fecha'];
    signo.temperatura = this.form.value['temperatura'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];

    if (this.edicion) {
      this.signoService.modificar(signo).subscribe( ()=> {
        this.signoService.listarPageable(0, 10).subscribe(data => {
          this.signoService.setSignoCambio(data);
          this.signoService.setMensajeCambio('SE MODIFICO');
        });
      });
    } else {
      this.signoService.registrar(signo).subscribe( ()=> {
        this.signoService.listarPageable(0, 10).subscribe(data => {
          this.signoService.setSignoCambio(data);
          this.signoService.setMensajeCambio('SE REGISTRO');
        });
      });
    }
    this.router.navigate(['pages/signo']);    
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  abrirDialogoNuevoPaciente() {
    this.dialog.open(PacienteDialogComponent, {
      width: '400px',
      data: this.pacienteSubject
    });
  }

}
