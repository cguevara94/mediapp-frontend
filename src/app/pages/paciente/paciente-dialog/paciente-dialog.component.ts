import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import { PacienteEdicionComponent } from '../paciente-edicion/paciente-edicion.component';

@Component({
  selector: 'app-paciente-dialog',
  templateUrl: './../paciente-edicion/paciente-edicion.component.html',
  styleUrls: ['./paciente-dialog.component.css']
})
export class PacienteDialogComponent extends PacienteEdicionComponent implements OnInit  {

  constructor(protected route: ActivatedRoute,
    protected router: Router,
    protected pacienteService: PacienteService, 
    private dialogRef: MatDialogRef<PacienteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Subject<Paciente>, 
    private snackBar: MatSnackBar) { 
      super(route, router, pacienteService);
    }

  ngOnInit(): void {
    super.limpiarForm();

    this.pacienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
    });
  }

  registrar() {
    this.pacienteService.registrar(this.obtenerInstanciaOperar()).pipe(switchMap(res => {
      var location = res.headers.get('location');
      return this.pacienteService.consultarLocation(location);
    })).subscribe(data => {
      this.data.next(data);
      this.pacienteService.setMensajeCambio('SE REGISTRO');
      this.cerrar();
    });
  }

  operar() {
    this.registrar();
  }

  cerrar() {
    this.dialogRef.close();
  }

  cancelar() {
    this.cerrar();
  }

}
