import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected pacienteService: PacienteService
  ) { }

  ngOnInit(): void {
    this.limpiarForm();

    this.route.params.subscribe((data: Params) => {
      console.log('trae data')
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    })
  }

  limpiarForm() {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'telefono': new FormControl(''),
      'direccion': new FormControl(''),
      'email': new FormControl('')
    });
  }

  initForm() {
    if (this.edicion) {
      this.pacienteService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idPaciente),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'dni': new FormControl(data.dni),
          'telefono': new FormControl(data.telefono),
          'direccion': new FormControl(data.direccion),
          'email': new FormControl(data.email)
        });
      });
    }
  }

obtenerInstanciaOperar() {
  let paciente = new Paciente();
    paciente.idPaciente = this.form.value['id'];
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dni = this.form.value['dni'];
    paciente.telefono = this.form.value['telefono'];
    paciente.direccion = this.form.value['direccion'];
    paciente.email = this.form.value['email'];
    return paciente;
}

modificar() {
  this.pacienteService.modificar(this.obtenerInstanciaOperar()).subscribe( ()=> {
    this.pacienteService.listar().subscribe(data => {
      this.pacienteService.setPacientecambio(data);
      this.pacienteService.setMensajeCambio('SE MODIFICO');
    });
  });
}

registrar() {
  this.pacienteService.registrar(this.obtenerInstanciaOperar()).subscribe( ()=> {
    this.pacienteService.listar().subscribe(data => {
      this.pacienteService.setPacientecambio(data);
      this.pacienteService.setMensajeCambio('SE REGISTRO');
    });
  });
}

  operar(): void {
    if (this.edicion) {
      //MODIFICAR
      this.modificar();
    } else {
      //REGISTRAR
      this.registrar();
    }
    this.redireccionar();
  }

  redireccionar() {
    this.router.navigate(['pages/paciente']);
  }

  cancelar() {
    this.redireccionar();
  }

}
