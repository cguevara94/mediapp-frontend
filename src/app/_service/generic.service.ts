import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Paciente } from '../_model/paciente';

@Injectable({
  providedIn: 'root'
})
export class GenericService<T> {

  private mensajeCambio: Subject<string> = new Subject<string>();

  constructor(
    protected http: HttpClient,
    protected url: string
  ) { }

  listar(){
    return this.http.get<T[]>(this.url);
  }

  listarPorId(id: number) {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  consultarLocation(location: string) {
    return this.http.get<T>(location);
  }

  registrar(t: T) : Observable<HttpResponse<Object>> {
    return this.http.post(this.url, t, {observe: 'response'});
  }

  modificar(t: T) {
    return this.http.put(this.url, t);
  }

  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }

  setMensajeCambio(mensaje: string){
    this.mensajeCambio.next(mensaje);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

}
