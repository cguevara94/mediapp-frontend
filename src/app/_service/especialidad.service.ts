import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Especialidad } from '../_model/especialidad';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService extends GenericService<Especialidad>{

  private especialidadCambio = new Subject<Especialidad[]>();

  constructor(protected http: HttpClient) {
    super(
      http,
      `${environment.HOST}/especialidades`);
  }

  //get Subjects
  getEspecialidadCambio() {
    return this.especialidadCambio.asObservable();
  }

  //set Subjects
  setEspecialidadCambio(especialdades: Especialidad[]) {
    this.especialidadCambio.next(especialdades);
  }

}
